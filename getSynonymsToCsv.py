
# Test requests API callers
import requests
import csv

### 
# Demo thesaurus API
# word = "provide"
# response = requests.get("http://words.bighugelabs.com/api/2/86250f4874f515dfa7f461cc2cd84b03/"+word+"/") # http://words.bighugelabs.com/api/{version}/{api key}/{word}/{format}
# print(response.text)

#Read from csv file
with open("TrainingSet - EqualWordsListFromReference.csv", "r") as f:
  content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
inputLines = [x.strip() for x in content]
# print(inputLines);

inputLineSplit = []

#Put the last word of every line into array inputWords
# TOP_N_INPUT_WORDS = 1000 # 2nd try # 1st try 891

# !!!!! Please update inputLines to appropriate numbers
for inputLine in inputLines[2856:]: # First try [:891] # 2nd try 857:1856 #3rd try 2856:
  a1 = inputLine.split(",")
  # print(a1);
  a2 = [a for a in a1 if a not in set([''])]  ## filter the empty substrings
  a3 = a2[-1] ## remove all except last word
  inputLineSplit += [a3] 

# remove all stop words
stopWords = set([ ",", ";", ":", "?", ".", "'", "a", "about", "above", "after", "again", "against", "all", "am", "an", "and", "any", "are", "as", "at", "be", "because", "been", "before", "being", "below", "between", "both", "but", "by", "could", "did", "do", "does", "doing", "down", "during", "each", "few", "for", "from", "further", "had", "has", "have", "having", "he", "he'd", "he'll", "he's", "her", "here", "here's", "hers", "herself", "him", "himself", "his", "how", "how's", "i", "i'd", "i'll", "i'm", "i've", "if", "in", "into", "is", "it", "it's", "its", "itself", "let's", "me", "more", "most", "my", "myself", "nor", "of", "on", "once", "only", "or", "other", "ought", "our", "ours", "ourselves", "out", "over", "own", "same", "she", "she'd", "she'll", "she's", "should", "so", "some", "such", "than", "that", "that's", "the", "their", "theirs", "them", "themselves", "then", "there", "there's", "these", "they", "they'd", "they'll", "they're", "they've", "this", "those", "through", "to", "too", "under", "until", "up", "very", "was", "we", "we'd", "we'll", "we're", "we've", "were", "what", "what's", "when", "when's", "where", "where's", "which", "while", "who", "who's", "whom", "why", "why's", "with", "would", "you", "you'd", "you'll", "you're", "you've", "your", "yours", "yourself", "yourselves" ])

# excludeWords are words we can exclude from any processing.
excludeWords = set([
  # "limited",  "lasted",  "managed",  "forget",  "mores",  "issued",  "lack",  "dynamic",  "focus",  "invent",  "existing",  "leads",  "known",  "instantiating",  "manages",  "fairness",  "go",  "follow",  "involved",  "hate",  "issues",  "improperly",  "injure",  "million",  "lining",  "father",  "misses",  "factoring",  "environment",  "helps",  "health",  "include",  "friendly",  "norm",  "labels",  "human-robot",  "far",  "maximized",  "engineered",  "morally",  "every",  "minded",  "fall",  "human-like",  "encounter",  "governments",  "level",  "experiencing",  "list",  "large",  "guiding",  "mistaking",  "enhance",  "inaction",  "heading",  "fearing",  "educators",  "machined",  "feeling",  "maintain",  "lethal",  "many",  "governor",  "functioned",  "growing",  "functioning",  "investment",  "machines",  "even",  "errors",  "evokes",  "forgetting",  "giving",  "marketed",  "fingers",  "expressed",  "networks",  "intelligences",  "evoked",  "experiment",  "misrepresent",  "goes",  "marketing",  "expresses",  "falling",  "increasing",  "ever",  "harassing",  "intends",  "lessons",  "focuses",  "led",  "gathered",  "entertains",  "equals",  "drew",  "entertainer",  "explore",  "entertain",  "let",  "groups",  "investigating",  "invented",  "internationals",  "moralities",  "engage",  "healthy",  "frustrating",  "inspired",  "larger",  "experience",  "fathering",  "justice",  "harass",  "mentor",  "natures",  "domains",  "mentions",  "implement",  "mistake",  "makes",  "fathered",  "involves",  "initials",  "lowers",  "examining",  "named",  "formulated",  "followed",  "morals",  "explained",  "logical",  "follower",  "formulates",  "names",  "market",  "languages",  "manufacturing",  "handled",  "entities",  "illegal",   "represent", "surrounded", "semantic", "pointing", "regulars", "pairing", "results", "themes", "slandered", "stops", "talks", "todays", "trusts", "reasoning", "poorly", "relationships", "suicide", "referring", "transferred", "technique", "scold", "safeties", "shall", "presents", "to", "topic", "program", "promoting", "those", "under", "teaching", "pleasing", "trolled", "resources", "supported", "rebukes", "risk", "righted", "rise", "parry", "rebuked", "putting", "servicing", "telling", "specific", "treating", "perceived", "scholar", "solves", "policeman", "skills", "turns", "socials", "solution", "standards", "specials", "solved", "race", "perceives", "shows", "stating", "programme", "still", "prevent", "parted", "pretend", "says", "punishing", "situates", "past", "surrounding", "second", "persuade", "pass", "twitters", "situated", "scientific", "plays", "preferences", "selected", "sooners", "shares", "scientists", "shared", "reproaching", "racing", "public", "scripted", "told", "specialist", "processing", "spacing", "understanding", "reported", "objects", "operating", "others", "represented", "triggering", "safes", "respect", "teacher", "paining", "publishes", "search", "revile", "restrictive", "receive", "shift", "technologists", "reports", "stupidity", "suggests", "products", "stimulating", "suggestion", "published", "opinion", "specialize", "quotient", "numbs", "offs", "raise", "partnering", "problematic", "siri", "prefer", "threes", "private", "outcomes", "psychological", "tay's", "reprimands", "shifting", "trained", "suicides", "rarely", "tools", "teach", "select", "sympathetic", "reflected", 
])

inputWords = [item for item in set(inputLineSplit) if item not in stopWords] # inputLineSplit - stopWords
inputWords = [item for item in set(inputLineSplit) if item not in excludeWords] # inputLineSplit - excludeWords

# print(inputWords)

allSynonymsForAllWords = [];

#Loop inputWords and callAPI
for word in inputWords: 
  print(word) # print word to show progress so far
  # call api, return response
  ## API Key 1: 86250f4874f515dfa7f461cc2cd84b03
  ## API Key 2: a984fd6afb9f863cde86d3334320badb
  response = requests.get("http://words.bighugelabs.com/api/2/3f60010ccc800a0b58dd05d90095a575/"+word+"/") # http://words.bighugelabs.com/api/{version}/{api key}/{word}/{format}
  
  # assume apiResponse exists
  apiResponseForWord = response.text

  # MOCK DATA DUE TO abide
  # apiResponseForWord = "verb|syn|bide\n" +"verb|syn|stay\n"+"verb|syn|digest\n"+"verb|syn|endure\n"+"verb|syn|stick out\n"+"verb|syn|stomach\n"+"verb|syn|bear\n"+"verb|syn|stand\n"+"verb|syn|tolerate\n"+"verb|syn|support\n"+"verb|syn|brook\n"+"verb|syn|suffer\n"+"verb|syn|put up\n"+"verb|syn|allow\n"+"verb|syn|continue\n"+"verb|syn|countenance\n"+"verb|syn|let\n"+"verb|syn|permit\n"+"verb|syn|remain\n"+"verb|syn|stay on"
  # todo get top x only

  # turn apiResponseForWord into array
  lines = apiResponseForWord.split("\n")
  # print(lines)

  synonyms = []
  synonyms.append(word)
  TOP_N_LINES = 4
  for line in lines[:TOP_N_LINES]: 
    _word = line.split("|")[-1] # get last word
    synonyms.append(_word)
    # print(_word)

  # print(synonyms) # for this word
  allSynonymsForAllWords.append(synonyms)

# Write to File
with open("output.csv", "wb") as f:
  writer = csv.writer(f)
  writer.writerows(allSynonymsForAllWords)

#The format of output is 
#
#verb|syn|bide
#verb|syn|stay
#verb|syn|digest
#verb|syn|endure
#verb|syn|stick out
#verb|syn|stomach
#verb|syn|bear
#verb|syn|stand
#verb|syn|tolerate
#verb|syn|support
#verb|syn|brook
#verb|syn|suffer
#verb|syn|put up
#verb|syn|allow
#verb|syn|continue
#verb|syn|countenance
#verb|syn|let
#verb|syn|permit
#verb|syn|remain
#verb|syn|stay on
#

# Get the last substring of each line