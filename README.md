# getSynonymsToCsv

Purpose: Get Synonyms To Csv by calling the API here

https://words.bighugelabs.com/admin/{token}

As of 30MAR, there is an API limit of 1000 words/day, so a different token must be used everytime it runs out.